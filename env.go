package env

import (
	"os"
	"strconv"
	"strings"
	"time"
)

// StringOrDoSomething gets an env variable as string or run a given function to get a default value (or panic).
func StringOrDoSomething(name string, something func() string) string {
	val := os.Getenv(name)
	if val == "" {
		return something()
	}
	return val
}

// IntOrDoSomething gets an env variable as int or run a given function to get a default value (or panic).
func IntOrDoSomething(name string, something func() int) int {
	val := os.Getenv(name)

	if val == "" {
		return something()
	}

	i, e := strconv.Atoi(val)
	if e != nil {
		return something()
	}

	return i
}

// BoolOrDoSomething gets an env variable as bool or run a given function to get a default value (or panic).
func BoolOrDoSomething(name string, something func() bool) bool {
	val := os.Getenv(name)
	if val == "" {
		return something()
	}

	switch strings.ToLower(val) {
	case "true", "1", "yes":
		return true
	default:
		return false
	}
}

// DurationOrDoSomething gets an env variable as duration or run a given function to get a default value (or panic).
func DurationOrDoSomething(name string, something func() time.Duration) time.Duration {
	val := os.Getenv(name)
	if val == "" {
		return something()
	}

	d, e := time.ParseDuration(val)
	if e != nil {
		return something()
	}

	return d
}

// Exists checks if the env variable exists.
func Exists(name string) bool {
	return os.Getenv(name) != ""
}

// String gets an env variable as string or return the given default value.
func String(name, value string) string {
	return StringOrDoSomething(name, func() string {
		return value
	})
}

// Int gets an env variable as int or return the given default value.
func Int(name string, value int) int {
	return IntOrDoSomething(name, func() int {
		return value
	})
}

// Bool gets an env variable as bool or return the given default value.
func Bool(name string, value bool) bool {
	return BoolOrDoSomething(name, func() bool {
		return value
	})
}

// Duration gets an env variable as duration or return the given default value.
func Duration(name string, value time.Duration) time.Duration {
	return DurationOrDoSomething(name, func() time.Duration {
		return value
	})
}

// StringOrPanic gets an env variable as string or panics if unset.
func StringOrPanic(name string) string {
	return StringOrDoSomething(name, func() string {
		panic(name + " env var is unset")
	})
}

// IntOrPanic gets an env variable as int or panics if unset or invalid.
func IntOrPanic(name string) int {
	return IntOrDoSomething(name, func() int {
		panic(name + " env var is expected to be int")
	})
}

// BoolOrPanic gets an env variable as bool or panics if unset.
func BoolOrPanic(name string) bool {
	return BoolOrDoSomething(name, func() bool {
		panic(name + " env var is unset")
	})
}

// DurationOrPanic gets an env variable as duration or panics if unset.
func DurationOrPanic(name string) time.Duration {
	return DurationOrDoSomething(name, func() time.Duration {
		panic(name + " env var is expected to be a valid duration")
	})
}
