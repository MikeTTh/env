package env

import (
	"testing"
	"time"
)

func TestExists(t *testing.T) {
	type TestCase struct {
		Name     string
		Defined  bool
		Val      string
		Expected bool
	}
	tcs := []TestCase{
		{
			Name:     "defined",
			Defined:  true,
			Val:      "asd",
			Expected: true,
		},
		{
			Name:     "defined but empty",
			Defined:  true,
			Val:      "",
			Expected: false,
		},
		{
			Name:     "undefined",
			Defined:  false,
			Expected: false,
		},
	}
	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			if tc.Defined {
				t.Setenv("TEST_ENVVAR", tc.Val)
			}
			result := Exists("TEST_ENVVAR")
			if result != tc.Expected {
				t.Errorf("Exists(%s) = %t; expected %t", tc.Val, result, tc.Expected)
			}
		})
	}
}

func TestString(t *testing.T) {
	type TestCase struct {
		Name     string
		Defined  bool
		Val      string
		Default  string
		Expected string
	}
	tcs := []TestCase{
		{
			Name:     "defined",
			Defined:  true,
			Val:      "asd",
			Default:  "",
			Expected: "asd",
		},
		{
			Name:     "defined but empty",
			Defined:  true,
			Val:      "",
			Default:  "",
			Expected: "",
		},
		{
			Name:     "undefined",
			Defined:  false,
			Default:  "",
			Expected: "",
		},
		{
			Name:     "defined but empty, default set",
			Defined:  true,
			Val:      "",
			Default:  "alma",
			Expected: "alma",
		},
		{
			Name:     "undefined, default set",
			Defined:  false,
			Default:  "alma",
			Expected: "alma",
		},
	}
	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			if tc.Defined {
				t.Setenv("TEST_ENVVAR", tc.Val)
			}
			result := String("TEST_ENVVAR", tc.Default)
			if result != tc.Expected {
				t.Errorf("String(%s, %s) = %s; expected %s", tc.Val, tc.Default, result, tc.Expected)
			}
		})
	}
}

func TestBool(t *testing.T) {
	type TestCase struct {
		Name     string
		Defined  bool
		Val      string
		Default  bool
		Expected bool
	}
	tcs := []TestCase{
		{
			Name:     "defined - truthy 1",
			Defined:  true,
			Val:      "yes",
			Default:  false,
			Expected: true,
		},
		{
			Name:     "defined - truthy 2",
			Defined:  true,
			Val:      "true",
			Default:  false,
			Expected: true,
		},
		{
			Name:     "defined - truthy 3",
			Defined:  true,
			Val:      "1",
			Default:  false,
			Expected: true,
		},
		{
			Name:     "defined - truthy 4",
			Defined:  true,
			Val:      "YES",
			Default:  false,
			Expected: true,
		},
		{
			Name:     "defined - truthy 5",
			Defined:  true,
			Val:      "TRUE",
			Default:  false,
			Expected: true,
		},
		{
			Name:     "defined - falsy",
			Defined:  true,
			Val:      "no",
			Default:  false,
			Expected: false,
		},
		{
			Name:     "defined - falsy but true as default",
			Defined:  true,
			Val:      "no",
			Default:  true,
			Expected: false,
		},
		{
			Name:     "defined - truthy but false as default",
			Defined:  true,
			Val:      "yes",
			Default:  false,
			Expected: true,
		},
		{
			Name:     "defined but empty and false as default",
			Defined:  true,
			Val:      "",
			Default:  false,
			Expected: false,
		},
		{
			Name:     "defined but empty and true as default",
			Defined:  true,
			Val:      "",
			Default:  true,
			Expected: true,
		},
		{
			Name:     "undefined and true as default",
			Defined:  false,
			Default:  true,
			Expected: true,
		},
		{
			Name:     "undefined and true as default",
			Defined:  false,
			Default:  false,
			Expected: false,
		},
	}
	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			if tc.Defined {
				t.Setenv("TEST_ENVVAR", tc.Val)
			}
			result := Bool("TEST_ENVVAR", tc.Default)
			if result != tc.Expected {
				t.Errorf("Bool(%s, %t) = %t; expected %t", tc.Val, tc.Default, result, tc.Expected)
			}
		})
	}
}

func TestInt(t *testing.T) {
	type TestCase struct {
		Name     string
		Defined  bool
		Val      string
		Default  int
		Expected int
	}
	tcs := []TestCase{
		{
			Name:     "defined not int",
			Defined:  true,
			Val:      "asd",
			Default:  12,
			Expected: 12,
		},
		{
			Name:     "defined yes int",
			Defined:  true,
			Val:      "32",
			Default:  12,
			Expected: 32,
		},
		{
			Name:     "defined but empty",
			Defined:  true,
			Val:      "",
			Default:  42,
			Expected: 42,
		},
		{
			Name:     "undefined",
			Defined:  false,
			Default:  500,
			Expected: 500,
		},
	}
	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			if tc.Defined {
				t.Setenv("TEST_ENVVAR", tc.Val)
			}
			result := Int("TEST_ENVVAR", tc.Default)
			if result != tc.Expected {
				t.Errorf("Int(%s, %d) = %d; expected %d", tc.Val, tc.Default, result, tc.Expected)
			}
		})
	}
}

func TestDuration(t *testing.T) {
	type TestCase struct {
		Name     string
		Defined  bool
		Val      string
		Default  time.Duration
		Expected time.Duration
	}
	tcs := []TestCase{
		{
			Name:     "defined not duration",
			Defined:  true,
			Val:      "asd",
			Default:  12,
			Expected: 12,
		},
		{
			Name:     "defined yes duration",
			Defined:  true,
			Val:      "32s",
			Default:  12,
			Expected: 32 * time.Second,
		},
		{
			Name:     "defined yes duration 2",
			Defined:  true,
			Val:      "3h10m32s",
			Default:  12,
			Expected: 11432 * time.Second,
		},
		{
			Name:     "defined but empty",
			Defined:  true,
			Val:      "",
			Default:  42,
			Expected: 42,
		},
		{
			Name:     "undefined",
			Defined:  false,
			Default:  500,
			Expected: 500,
		},
	}
	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			if tc.Defined {
				t.Setenv("TEST_ENVVAR", tc.Val)
			}
			result := Duration("TEST_ENVVAR", tc.Default)
			if result != tc.Expected {
				t.Errorf("Duration(%s, %d) = %d; expected %d", tc.Val, tc.Default, result, tc.Expected)
			}
		})
	}
}
